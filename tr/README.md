# Ev yapımı diş macunu

This homemade toothpaste is remineralizing and contains no chemicals, no SLS, no fluoride.

## Ingredients

 - 25 g. coconut oil
 - 1 tbsp. baking powder
 - 1 tbsp. sea salt
 - 1 tbsp. calcium magnesium powder (dolomite)
 - 1 tbsp. xylit
 - 12-15 drops peppermint essential oil
 
## Directions

 1. Melt coconut oil giving it a bit heat. It melts over 26 °C.
 2. Mix all powdery ingredients (baking powder, sea salt, dolomite and xylit) in a separate cup.
 3. Put the powder-mix into coconut oil and stir until it gets quite homogeneous.
 4. Lastly put 12 to 15 drops of peppermint essential oil into the mix.
 5. Stir continuously but slowly until it gets a pasty texture. It can take 5-10 minutes depending on the room 
temperature.
 6. Put the paste in a closed container and you are good to go.
 
## Usage
 
 - Scratch the paste with your toothbrush and brush your teeth.

## Recommendations

 - Brush your teeth twice a day, mornings and evenings before going to bed.
 - If it is a warm day, coconut oil will be fluid and the other ingredients can precipitate. If so, you should stir it a
bit to make it homogeneous again.
 - If it is a cold day, it will be very hard, then you need to scratch it a bit more than usual.
