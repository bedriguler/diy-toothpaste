# Selbstgemachte Zahnpasta

Diese selbstgemachte Zahnpasta ist remineralisierend und enthält keine Chemikalien, kein SLS, kein fluoride.

## Zutaten

 - 25 g. Kokosfett
 - 1 EL Backpulver
 - 1 EL Meersalz
 - 1 EL Kalzium-Magnesium-Pulver (Dolomit)
 - 1 EL Xylit
 - 12-15 Tropfen ätherisches Pfefferminzöl
 
## Anleitung

 1. Kokosfett mit Hitze schmelzen lassen. Es schmilzt über 26 °C.
 2. Alle pulverige Zutaten zusammenmischen (Backpulver, Meersalz, Dolomit und Xylit) in einer separaten Schale.
 3. Die Pulvermischung in Kokosfett mischen und rühren bis es ziemlich homogen wird.
 4. Schließlich 12 bis 15 Tropfen ätherisches Pfefferminzöl in die Mischung eingeben.
 5. Kontinuierlich aber langsam einrühren bis es eine pastige Textur bekommt. Es kann 5 bis 10 Minuten dauern, je nach
Zimmertemperatur.
 6. Die Paste in einem geschlossenen Behälter einfüllen und fertig.
 
## Verwendung
 
 - Scratch the paste with your toothbrush and brush your teeth.

## Empfehlungen

 - Brush your teeth twice a day, mornings and evenings before going to bed.
 - If it is a warm day, coconut oil will be fluid and the other ingredients can precipitate. If so, you should stir it a
bit to make it homogeneous again.
 - If it is a cold day, it will be very hard, then you need to scratch it a bit more than usual.
